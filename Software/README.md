目录结构如下

- DataBase: 数据库设计

- Server: 使用SpringBoot框架开发的服务端
  - src: 源码目录
  - pom.xml: Maven项目配置文件

- Client: Android客户端
  - app: 主程序目录
    - src: 源码目录
    - libs: 依赖库目录
    - build.gradle: 主程序Gradle配置文件
  - build.gradle: Gradle项目配置文件

- FaceRecognition: 人脸识别算法
  - under construction
