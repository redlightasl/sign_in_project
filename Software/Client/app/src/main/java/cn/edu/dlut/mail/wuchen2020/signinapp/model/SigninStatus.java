package cn.edu.dlut.mail.wuchen2020.signinapp.model;

public class SigninStatus {
    private Course course;
    private Integer status;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
