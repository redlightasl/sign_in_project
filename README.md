# sign_in_project
A single project in "National College Student Innovation and Entrepreneurship Training Program"

## Screenshots
<details>
<summary>Click to view</summary>

![Server1](./images/server-1.png)
![Server2](./images/server-2.png)
![Client1](./images/android-1.jpg)
![Client2](./images/android-2.jpg)
![Embedded1](./images/embedded-1.jpg)
![Embedded2](./images/embedded-2.jpg)
![SigninFSM](./images/fsm.png)

</details>

## General
This is an experimental software and hardware co-design solution for students signing-in.
Aiming at college, we tried to achieve convenient check-in and check-out service through face recognition.

### Hardware solution
Based on STM32H7xx high performance MCU and ESP32 WiFi&BLE module, we developed a set of **demo board** plan and a test-plan with **evaluation board**. We were expected to design a set of PCB for this programme in the future.

### Software solution
The server backend is based on **SpringBoot**+**Hibernate**+**MySQL** and the frontend is based on **Vue.js**. It is deployed on linux. We also develop an **Android** APP as the mobile client.

## Schedule and Backlogs
`Project Framework` √

`Presentation 1` √

`Presentation 2` ×

`Presentation 3` ×

`Final Submit` ×

## Project Structure
`Hardware` SPI communication between ESP32 and STM32

`Software` Database, server, client design and face recognition algorithm

`report` Project reports

## Build Requirements
    JDK>=1.8
    Android Studio==4.1.3
    Android SDK==24.4.1
    Python==3.7.0
